Hello everyone, and welcome to our **The Bridge** issue board!
This is an internal tool that allows smoother interaction between the development team, the beta testers and the staff team. 

To get started, visit [this link](https://gitlab.com/insanemc-public/thebridge/-/issues)!

![pic0][https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DoFPz8Riag6w&psig=AOvVaw11cu_8nNDaW4WXF4k8WQZi&ust=1597507852620000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCMj5vdSKm-sCFQAAAAAdAAAAABA1]
